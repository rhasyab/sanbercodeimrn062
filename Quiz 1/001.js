// A.

var num1
var num2

function bandingkan(num1, num2) {

    if(num1 < num2) {
        return num2
    }

    else if(num1 > num2) {
        return num1
    }

    else if(num1 == num2) {
        return -1
    }

    else if(num1 == 1 || num2 == undefined) {
        return 1
    }

    else{
        return -1
    }

}

// B.

function balikString(str) {

    var strBaru = ""
    
    for(var i = str.length - 1; i >= 0; i--){
        strBaru += str[i]
    }
    return strBaru
}

// C.

function palindrome(str) {

    strBaru = ""
    
    for(i = str.length - 1; i >= 0; i--) {
        strBaru += str[i]
    }

    if(str == strBaru) {
        return "true"
    }

    else {
        return "false"
    }
}

console.log('Jawaban A.')
// TEST CASES Bandingkan Angka
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 

console.log("\n")
console.log('Jawaban B.')

// TEST CASES BalikString
console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah

console.log("\n")
console.log('Jawaban C.')

// TEST CASES Palindrome
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false