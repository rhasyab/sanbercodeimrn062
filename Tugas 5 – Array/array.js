// Soal No. 1
console.log("<-- Soal No. 1 -->")

var startNum
var finishNum

function range(startNum, finishNum) {

    var array = []
    var i

    if (startNum < finishNum) {
        for(i = startNum; i <= finishNum; i++) {
            array.push(i)
        }
    }

    else if (startNum > finishNum) {
        for(i = startNum; i >= finishNum; i--) {
            array.push(i)
        }
    }

    else {
        array = console.log(-1)
    }
    
    return array

}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

// Soal No. 2
console.log("\n")
console.log("<-- Soal No. 2 -->")

var step

function rangeWithStep(startNum, finishNum, step) {

    var array2 = []

    if (startNum < finishNum) {
        for(i = startNum; i <= finishNum; i = i + step) {
            array2.push(i)
        }
    }

    else if (startNum > finishNum) {
        for(i = startNum; i >= finishNum; i = i - step) {
            array2.push(i)
        }
    }

    return array2
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 


// Soal No. 3
console.log("\n")
console.log("<-- Soal No. 3 -->")

var awal
var akhir
var langkah
var total = 0

function sum(awal, akhir, langkah) {

    var array3 = []

        if (awal < akhir) {
            for(i = awal; i <= akhir; i = i + langkah) {
                array3.pop(i);
                array3.push(total+=i);
            }
        }
    
        else if (awal > akhir) {
            for(i = awal; i >= akhir; i = i - langkah) {
                array3.pop(i);
                array3.push(total+=i);
            }
        }

    return total
}

console.log(sum(1, 10, 1)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15, 10)) // 75
console.log(sum(20, 10)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

// Soal No. 4
console.log("\n")
console.log("<-- Soal No. 4 -->")

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling() {
    for(var baris = 0; baris < 4; baris++) {
        for(var kolom = 0; kolom < 5; kolom++) {
            if (kolom == 0) {
                console.log("Nomor ID: " + input[baris][kolom])
            }
            
            else if (kolom == 1) {
                console.log("Nama Lengkap: " + input[baris][kolom])
            }
            
            else if (kolom == 3) {
                console.log("TTL: " + input[baris][kolom])
            }
            
            else if (kolom == 4) {
                console.log("Hobi: " + input[baris][kolom])   
            }
        }
        console.log("\n")
    }
}

dataHandling();

// Soal No. 5
console.log("<-- Soal No. 5 -->")

function balikKata(str) {
    var newString = "";
 
    for (var i = str.length - 1; i >= 0; i--) { 
        newString += str[i];
    }
    return newString;
}
 
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

// Soal No. 6
console.log("\n")
console.log("<-- Soal No. 6 -->")

var input = ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Membaca"];

function dataHandling2(input) {

    input.splice(4, 0, "Pria", "SMA Internasional Metro")
    input.splice(6, 1)
    console.log(input)

    var copyInput = input.slice()
    copyInput.splice(0,1)
    // console.log(copyInput)
    copyInput.splice(1)


    var tgl = input[3].split("/")
    // console.log(tgl)

    var copyTgl = tgl.slice()
    copyTgl.sort()

    var joinTgl = tgl.join("-")

    tgl.splice(0,1)
    tgl.splice(1,1)
    bln = Number(tgl)

    // console.log(copyTgl)

    switch (bln) {
        case 01:
            console.log("Januari")
        break;
        
        case 02:
            console.log("Februari")
        break;

        case 03:
            console.log("Maret")
        break;

        case 04:
            console.log("April")
        break;
        
        case 05:
            console.log("Mei")
        break;

        case 06:
            console.log("Juni")
        break;

        case 07:
            console.log("Juli")
        break;
        
        case 08:
            console.log("Agustus")
        break;

        case 09:
            console.log("September")
        break;

        case 10:
            console.log("Oktober")
        break;
        
        case 11:
            console.log("November")
        break;

        case '12':
            console.log("Desember")
        break;

        default:
            break;
    }

    console.log(copyTgl)
    console.log(joinTgl)
    console.log(copyInput)

}

dataHandling2(input);

/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 

