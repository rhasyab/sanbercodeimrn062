console.log('Soal No. 1 (Array to object)')

function arrayToObject(arr) {
    
    var now = new Date();
    var thisYear = now.getFullYear(); // 2020 (tahun sekarang)

    if(arr.length == 0) {
        console.log('""');
    } else {
        for(var i = 0; i < 2; i++) {
            var umur;
        
            if(arr[i][3] === null || arr[i][3] == undefined || arr[i][3] > thisYear) {
                var tahun = arr[i][3];
                umur = thisYear - tahun
                console.log(i + 1 + '. ' + arr[i][0] + ' ' + arr[i][1])
                
                var data = {};
                data.firstName = arr[i][0];
                data.lastName  = arr[i][1];
                data.gender    = arr[i][2];
                data.age       = 'Invalid Birth Year';

                console.log(data);
            } else {
                umur = thisYear - arr[i][3];

                console.log(i + 1 + '. ' + arr[i][0] + ' ' + arr[i][1]);

                var data = {};
                data.firstName = arr[i][0];
                data.lastName  = arr[i][1];
                data.gender    = arr[i][2];
                data.age       = umur;

                console.log(data);
            }
        }
    }
}
 
// Driver Code
var people = [ 
                ['Bruce', 'Banner', 'male', 1975], 
                ['Natasha', 'Romanoff', 'female' ] 
             ];

arrayToObject(people);

console.log('\n');

var people2 = [ 
                ['Tony', 'Stark', 'male', 1980], 
                ['Pepper', 'Pots', 'female', 2023] 
              ];
arrayToObject(people2);

console.log('\n');

// Error case 
arrayToObject([]); // ''

console.log('\n');
console.log('Soal No. 2 (Shopping time)');

var money;

function shoppingTime(memberId, money) {

    var produk = [];
    var member = memberId;
    var uang  = money;

    for(i = 0; i < 4; i++) {

        if(memberId == '' || money == undefined) {
            return 'Mohon maaf, toko X hanya berlaku untuk member saja';
        } else if(money < 50000) {
            return 'Mohon maaf, uang tidak cukup';
        } else { 
            if(money > 50000) {
                //Casing
                produk.push('Casing Handphone');
                uang -= 50000;

                if(uang >= 175000) {
                    //Sweater
                    produk.push('Sweater Uniklooh');
                    uang -= 175000;

                    if(uang >= 250000) {
                        //Baju H&N
                        produk.push('Baju H&N');
                        uang -= 250000;
                        
                        if(uang >= 500000) {
                            //Baju Zoro
                            produk.push('Baju Zoro');
                            uang -= 500000;

                            if(uang >= 1500000) {
                                //Sepatu
                                produk.push('Sepatu Stacattu');
                                uang -= 1500000;

                                var belanja = {};
                                belanja.memberId      = memberId;
                                belanja.money         = money;
                                belanja.listPurchased = produk;
                                belanja.changeMoney   = uang;
                                return belanja;
                            } 
                            
                        } else {
                            //Sweater Unikloh
                            var belanja = {};
                            belanja.memberId      = memberId;
                            belanja.money         = money;
                            belanja.listPurchased = produk;
                            belanja.changeMoney   = uang;
                            return belanja;
                        }
                    } else {
                        //Baju H&N
                        var belanja = {};
                        belanja.memberId      = memberId;
                        belanja.money         = money;
                        belanja.listPurchased = produk;
                        belanja.changeMoney   = uang;
                        return belanja;
                    }
                } else {
                    //Baju Zoro
                    var belanja = {};
                    belanja.memberId      = memberId;
                    belanja.money         = money;
                    belanja.listPurchased = produk;
                    belanja.changeMoney   = uang;
                    return belanja;
                }   
            } 
        }
    }
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); //Mohon maaf, toko X hanya berlaku untuk member saja

console.log('\n')
console.log('Soal No. 3 (Naik angkot)')

function naikAngkot(arrPenumpang) {
    var arPen = arrPenumpang;
    var arJaw = [];
    var index1;
    var index2;
    var hitung;
    var harga;

    rute = ['A', 'B', 'C', 'D', 'E', 'F'];

    if(arPen.length == 0) {
        return '""';
    } else { 
        for(var i = 0; i < 2; i++) {
            index1 = rute.indexOf(arPen[i][1]);
            index2 = rute.indexOf(arPen[i][2]);
            hitung = index2 - index1;
            harga  = hitung * 2000;
    
            var user = {};
            user.penumpang = arPen[i][0];
            user.naikDari  = arPen[i][1];
            user.tujuan    = arPen[i][2];
            user.bayar     = harga;
    
            arJaw.push(user);
        }   
        return arJaw;
    }
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
console.log(naikAngkot([])); //[]