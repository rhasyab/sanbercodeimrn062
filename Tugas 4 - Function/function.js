// Tugas No. 1
console.log("<-- Tugas No. 1 -->")
function teriak(){
    console.log("Halo Sanbers!")
}

console.log(teriak())

// Tugas No. 2
console.log("\n")
console.log("<-- Tugas No. 2 -->")

function kalikan (num1, num2) {
    return num1 * num2
}
 
var num1 = 12
var num2 = 4
 
var hasilKali = kalikan(num1, num2)
console.log(hasilKali)

// Tugas No. 3
console.log("\n")
console.log("<-- Tugas No. 3 -->")

function introduce(name, age, address, hobby) {
    return "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby + "!"
}

var name = "Rhasya"
var age = 19
var address = "BSD, Tangerang Selatan"
var hobby = "Design, Blogging, dan Coding"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!" 
